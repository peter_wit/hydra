package muic.opl.app

import org.scalatra.test.scalatest._
import org.scalatest.FunSuiteLike

class HydraServletTests extends ScalatraSuite with FunSuiteLike {

  addServlet(classOf[HydraServlet], "/*")

  test("GET / on HydraServlet should return status 200"){
    get("/"){
      status should equal (200)
    }
  }

}
