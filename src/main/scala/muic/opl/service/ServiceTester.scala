package muic.opl.service

import muic.opl.hash.HashCode
import muic.opl.model.{Coordinate, Node}

/**
  * NOT TO BE TESTED: Config
  * Database Service -> tested by peter
  * Sharding Service -> tested
  * Cluster Service -> tested
  * TO TEST: JsonService, CommunicationService
  */
object ServiceTester {
//  val clusterService = new ClusterService
//  val shardingService = new ShardingService with HashCode
//  val cluster = clusterService.createMasterCluster()
//  clusterService.addSlave(cluster, Node("111111","99"))
//  clusterService.addSlave(cluster, Node("111111","99"))
//  clusterService.addSlave(cluster, Node("2394","99"))
//  clusterService.addSlave(cluster, Node("111111","99"))
//  clusterService.addSlave(cluster, Node("414151","99"))
//  clusterService.addSlave(cluster, Node("1234","99"))
//  clusterService.addSlave(cluster, Node("1123111","100"))
//  println(cluster.getSlaves)
//  clusterService.removeSlave(cluster, Node("111111","99"))
//  clusterService.removeSlave(cluster, Node("1234","99"))
//  clusterService.removeSlave(cluster, Node("1281983279r","99"))
//  println(cluster.getSlaves)
//  println(shardingService.determineRecordLocation(Coordinate(3.23,1.123), cluster.getSlaves.toSeq))
}
