package muic.opl.service
//import scala.io.
import muic.opl.model.Coordinate
import scala.io.Source
import java.io.File
import java.io.PrintWriter

/**
  * Format of the config file
  * coord node,hash node,hash node,hash
  * coord node,hash node,hash node,hash
  * TODO: Bug testing
  */
class ConfigService {
  /**
    * Write into the file in such a way that we can retrieve it easily
    * @param hashTable
    * @return
    */
  def writeIntoFile(hashTable: Map[Coordinate, Vector[(Int,BigInt)]]) = {
    val writer = new PrintWriter(new File("ConfigFile.txt"))
    hashTable.keySet.foreach( coord => writer.write(constructRecordLine(coord,hashTable)))
  }


  /**
    * coord node, node, node
    * coord node, node, node
    * node is in NodeID,HashValue
    * coord is in latitude,longtitude
    * into
    * Map[Coordinate, Vector[(Int,BigInt)] ]
    * @return
    */
  def readFromFile(): Map[Coordinate, Vector[(Int,BigInt)]] = {

    val lines = Source.fromFile("ConfigFile.txt").getLines()
    val arrmap = lines
      .map(_.split(" "))
      .map(
        arr => arr.take(1).toString.split(",")
          -> arr.drop(1).map(str => str.split(",")))
    val toreturn = arrmap
      .map(
        arrs => Coordinate(arrs._1.head.toDouble,arrs._1.tail.head.toDouble)
          -> arrs._2.map(str => (str.head.toInt,BigInt(str.tail.head)))
      ).toMap
    toreturn.map(item => item._1 -> toreturn(item._1).toVector)
  }


  /**
    * God bless stringbuilder
    * @param coord
    * @param hashTable
    * @return
    */
  def constructRecordLine(coord: Coordinate, hashTable: Map[Coordinate, Vector[(Int,BigInt)]]): String = {
    val toReturn = new StringBuilder
    toReturn.append(coord.latitude + "," + coord.longitude + " ")
    hashTable(coord).foreach(item => toReturn.append(item._1 + "," + item._2 + " "))
    toReturn.toString()
  }
}
