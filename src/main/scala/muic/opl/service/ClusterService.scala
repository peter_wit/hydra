package muic.opl.service


import java.net._

import muic.opl.app.ServletItem
import muic.opl.model.{Cluster, Coordinate, Node}
import scala.collection._


class ClusterService {
  /**
    * create cluster with the local machine as the main cluster
    * Basically what happen here is:
    * 1) master create the config file and initialize cluster with empty nodes except itself
    * 2) master call the sequence of ips given in the input /cluster/join/slave with its ip as one of the param
    * 3) the slaves (if it's alive) will call the /cluster/join/master with its ip once again and through that way
    * the master will give it back a config file, a certificate that it belongs to the cluster
    */
  def createMasterCluster(): Cluster = {
    //set up the cluster before sending out the request to tell the slves to join.
    Cluster(mutable.HashSet(), Vector(), Node(ServletItem.ip,ServletItem.port.toString), 101, 100)
  }
  def createSlaveCluster(master: Node): Cluster = {
    Cluster(null, Vector(), master, -1, -1)
  }

  /**
    * Add slaves and remove slaves are synchronized because there's no concurrent HashSet
    */

  def addSlave(cluster: Cluster, node: Node): Unit = {
    this.synchronized {
      if (!cluster.getSlaves.contains(node)){
        cluster.getSlaves += node
        cluster.setNextNodeID(cluster.getNextNodeID + 1)
      }
   }
  }

  def removeSlave(cluster: Cluster, node: Node): Unit = {
    this.synchronized {
      cluster.getSlaves -= node
    }
  }

  /**
    * Not used yet
    * Add/remove coordinates
    */
//  def addCoordinate(cluster: Cluster, coord: Coordinate) = {
//    new Cluster(cluster.getSlaves, (coord::cluster.getCoordinates.toList).toVector, cluster.getMasterIP, cluster.getNextNodeID, cluster.getCurrentNodeID)
//  }
//
//  def removeCoordinate(cluster: Cluster, coord: Coordinate) = {
//    new Cluster(cluster.getSlaves, cluster.getCoordinates.filter(_ != coord), cluster.getMasterIP, cluster.getNextNodeID, cluster.getCurrentNodeID)
//  }
}

