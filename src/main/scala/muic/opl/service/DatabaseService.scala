package muic.opl.service

import muic.opl.model.{Coordinate, Database, Direction, Record}

class DatabaseService {
  def insert(record: Record): Unit = Database.insert(record)

  def get(coordinate: Coordinate): Option[Record] = Database.get(coordinate)

  def geoBox(northWest: Coordinate, southEast: Coordinate): List[Record] = Database.findWithinBox(northWest, southEast)

  def geoRadius(centre: Coordinate, radius: Double): List[Record] = Database.findWithinCircle(centre, radius)

  def geoStrip(coordinate1: Coordinate, coordinate2: Coordinate, direction: Direction): List[Record] =
    Database.findWithinStrip(coordinate1, coordinate2, direction)
}
