package muic.opl.service

import muic.opl.model.Record
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, Formats}
import org.json4s.jackson.JsonMethods.{compact, render}
import muic.opl.model.Node

class JsonService {
  implicit val jsonFormats: Formats = DefaultFormats
  def makeJsonFromNode(node: Node): String = {
    val json = ("ip" -> node.ip) ~ ("port" -> node.port)
    compact(render(json))
  }
  def makeJsonFromCoordinateAndPayload(coordinate: String, payload: List[String]): String = {
    val json = coordinate ->  payload
    compact(render(json))
  }

  def parseResponses(res: String): (String,List[(String,String)]) = {
    val jValue = parse(res)
    val recordString = jValue.extract[(String, List[(String,String)])]
    recordString
  }

  def parseResponse(res: String): (String,String) = {
    val jValue = parse(res)
    val recordString = jValue.extract[(String, String)]
    recordString
  }




  /**
    * records: List[(String, List(String))]
    * @param records
    * @return
    */
  def makeJsonFromRecords(records: List[Record]): String = {
    val json = "records" -> records.map(r => r.coordinate.toString -> r.payLoad.map(_.toChar).mkString)
    compact(render(json))
  }
  def makeJsonFromRecordsString(records: List[(String,String)]): String = {
    val json = "records" -> records
    compact(render(json))
  }

  def makeJsonFromRecord(record: Record): String = {
    val json = "record" -> ((record.coordinate.latitude + "," + record.coordinate.longitude) -> record.payLoad.map(_.toChar).mkString)
    compact(render(json))
  }

  def makeSuccess(): String = compact(render("result" -> "success"))
  def makeFailure(msg: String): String = {
    val str = "failure: " + msg
    compact(render("result" -> str))
  }
}
