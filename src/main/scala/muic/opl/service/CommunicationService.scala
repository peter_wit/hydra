package muic.opl.service

import java.net.InetAddress

import muic.opl.app.ServletItem
import org.apache.http.client.methods.{HttpGet, HttpPost, HttpUriRequest}
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import muic.opl.model.Node
import org.apache.http.HttpResponse

import scala.io.Source


class CommunicationService {
//  val genmap = Source
//    .fromResource("general")
//    .getLines().map(l => l.split(","))
//    .map(arr => arr.head -> arr.tail.head)
//    .toMap
//  val port = genmap("port")
  /**
    * 1) Get the port num from the property file
    * 2) construct the HttpPost (we use string format to do this
    * 3) set header to say that we are sending json body
    * 4) create Json body with the master ip and then set it into the post body
    * 5) send the post body out
    * TODO: Make the response not wait for any response -> make sure that timeout doesnt affect the performance of the application
    * @param node
    */
  def checkValidNode(node: Node): Unit = {
    val hostip = ServletItem.ip
    val hostport = ServletItem.port.toString
    val ip = node.ip
    val port = node.port
    val post = new HttpPost(s"http://$ip:$port/cluster/join/slave")
    post.setHeader("Content-type", "application/json")
    post.setEntity(new StringEntity(ServletItem.jsonService.makeJsonFromNode(Node(hostip,hostport))))
    val client = HttpClientBuilder.create().build()
    client.execute(post)
    client.close()
  }

  def reportToMaster(masternode: Node): Unit  = {
    val ip = masternode.ip
    val port = masternode.port
    val post = new HttpPost(s"http://$ip:$port/cluster/join/master")
    post.setHeader("Content-type", "application/json")
    post.setEntity(new StringEntity(ServletItem.jsonService.makeJsonFromNode(Node(ServletItem.ip,ServletItem.port.toString))))
    val client = HttpClientBuilder.create().build()
    client.execute(post)
    client.close()
  }

  def sendDatabaseGeoradiusRequestToSlaves(node: Node, location: String, radius: String): List[(String,String)] = {
    val ip = node.ip
    val port = node.port
    val api = "/database/georadius"
    val get = new HttpGet(s"http://$ip:$port$api")
    get.setHeader("location",location)
    get.setHeader("radius",radius)
    val responseBody = sendRequest(get)
    if(!responseBody.isEmpty){

      val check = responseBody.take(10).contains("result")
      if (check){
        List()
      }
      else{
        val (one,two) = ServletItem.jsonService.parseResponses(responseBody)
        two
      }
    }else{
      List()
    }
  }

  def sendDatabaseGeoboxRequestToSlaves(node: Node, location1: String, location2: String): List[(String,String)] = {
    val ip = node.ip
    val port = node.port
    val api = "/database/geobox"
    val get = new HttpGet(s"http://$ip:$port$api")
    get.setHeader("location1",location1)
    get.setHeader("location2",location2)
    val responseBody = sendRequest(get)
    if(!responseBody.isEmpty){
      //println("checking if it's result")
      //println("response from " + s"http://$ip:$port$api" + " -> " + responseBody)
      val check = responseBody.take(10).contains("result")
      if (check){
        List()
      }
      else{
        //println("parsing response")
        val (one,two) = ServletItem.jsonService.parseResponses(responseBody)
        //println("did not die at parsing")
        two
      }
    }else{
      List()
    }
  }

  def sendDatabaseGetRequestToSlaves(node: Node, location: String ): String = {
    val ip = node.ip
    val port = node.port
    val api = "/database/get"
    val get = new HttpGet(s"http://$ip:$port$api")
    //println("location setting header: " + location + " to " + s"http://$ip:$port$api")
    get.setHeader("location",location)
    val responseBody = sendRequest(get)
    //println(responseBody)
    if(!responseBody.isEmpty){
      val check = responseBody.take(10).contains("result")
      if (check)
        ""
      else
        responseBody
    }else{
      ""
    }
  }

  def sendDatabaseGeostripRequestToSlaves(node: Node, location1: String, location2: String, direction: String): List[(String,String)] = {
    val ip = node.ip
    val port = node.port
    val api = "/database/geostrip"
    val get = new HttpGet(s"http://$ip:$port$api")
    get.setHeader("location1",location1)
    get.setHeader("location2",location2)
    get.setHeader("direction",direction)
    val responseBody = sendRequest(get)
    if(!responseBody.isEmpty){
      val (one,two) = ServletItem.jsonService.parseResponses(responseBody)
      if (one == "result")
        List()
      else
        two
    }else{
      List()
    }
  }




  /**
    * send a single record to the node
    * @param node
    * @param coordinate
    * @param payload
    * @return
    */
  def sendRecordToNode(node: Node, coordinate: String,  payload: List[String]): Unit  = {
    val ip = node.ip
    val port = node.port
    val api = "/database/add"
    val post = new HttpPost(s"http://$ip:$port$api")
    post.setHeader("Content-type", "application/json")
    post.setEntity(new StringEntity(ServletItem.jsonService.makeJsonFromCoordinateAndPayload(coordinate,payload)))
    sendRequest(post)
  }

  def sendRequest(req: HttpUriRequest): String = {
    val client = HttpClientBuilder.create().build()
    val res = client.execute(req)
    val result = scala.io.Source.fromInputStream(res.getEntity.getContent).mkString
    client.close()
    result
  }

//  def sendRecordsToNode(ip: String,recordString: List[String])  = {
//    val post = new HttpPost(s"$ip:$port/database/add")
//    post.setHeader("Content-type", "application/json")
//    post.setEntity(new StringEntity(ServletItem.jsonService.makeRecords(recordString)))
//    val client = HttpClientBuilder.create().build()
//    client.execute(post)
//  }
}
