package muic.opl.service

import muic.opl.app.ServletItem
import muic.opl.model.{Coordinate, Node}
import muic.opl.hash.{HashCode, HashService}
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder

import scala.io.Source

/**
  * TODO:
  * 1) Allows people to specify which hashing function to use (maybe during the set up) -> Done
  * 2) Testing
  */
class ShardingService {
  hasher: HashService =>
  /**
    * Below is not used
    */
  //  /**
//    * Given: Sequence of Coordinate, All the nodes number
//    * To return: Map[coord : Map[nodeId : BigInt ] ]
//    */
//  def createHashTable(coords: Seq[Coordinate], nodes: Seq[Int]): Map[Coordinate, Vector[(Int,BigInt)]] = {
//    coords.map(c => c -> nodes.map(n => n -> BigInt(hasher.hash64(createStringToHash(c,n)))).toVector.sortBy(a => -a._2)).toMap
//  }
//
//
//  /**
//    * Given the hashtable, the coords and the nodes -> find the top few specified (default = 2)
//    * @param hashTable
//    */
//  def createDesignatedNodes(hashTable: Map[Coordinate, Vector[(Int,BigInt)]], top: Int = 2): Map[Coordinate,Vector[(Int,BigInt)]] =  {
//    hashTable.keySet.map(k => k -> hashTable(k).take(top)).toMap
//  }
//
//  /**
//    * drop all the item in the vector with the node id
//    * @param nodeID
//    * @param hashTable
//    * @return
//    */
//  def dropNodeFromHashTable(nodeID: Int, hashTable: Map[Coordinate, Vector[(Int,BigInt)]]): Map[Coordinate, Vector[(Int,BigInt)]] = {
//    hashTable.keySet.map(c => c -> hashTable(c).filter(_._1 != nodeID)).toMap
//  }
//
//  /**
//    * drop the entire vector of that coordinate from the hash table
//    * @param coord
//    * @param hashTable
//    * @return
//    */
//  def dropCoordinateFromHashTable(coord: Coordinate, hashTable: Map[Coordinate, Vector[(Int,BigInt)]]): Map[Coordinate, Vector[(Int,BigInt)]] = {
//    hashTable.filter(a => a._1 != coord)
//  }
//
//  /**
//    * as the name suggest, for each coordinate get the vector and create the hash for that item and add that tuple into the vector
//    * @param nodeID
//    * @param hashTable
//    * @return
//    */
//  def addNodeIntoHashTable(nodeID: Int , hashTable: Map[Coordinate, Vector[(Int,BigInt)]]): Map[Coordinate, Vector[(Int,BigInt)]] = {
//    hashTable
//      .keySet
//      .map(coord =>
//        coord -> (
//            (nodeID,BigInt(hasher.hash64(createStringToHash(coord,nodeID))))
//              ::
//              hashTable(coord)
//                .toList)
//          .toVector)
//      .toMap
//  }
//
//  /**
//    * add another record (coordinate into the table)
//    * @param c
//    * @param nodes
//    * @param hashTable
//    * @return
//    */
//  def addCoordIntoHashTable(c: Coordinate, nodes: Seq[Int], hashTable: Map[Coordinate, Vector[(Int,BigInt)]]): Map[Coordinate, Vector[(Int,BigInt)]] = {
//    hashTable + (c -> nodes.map(n => (n,BigInt(hasher.hash64(createStringToHash(c,n))))).toVector)
//  }

  /**
    * Make ourlives easy we cut out all the top part because it's too much to debug and go easy route
    * @param c
    * @param nodes
    * @return
    */
  def determineRecordLocation(c: Coordinate, nodes: Seq[Node]): Seq[Node] = {
    nodes.map(a => a -> createStringToHash(c,a.toString)).map(a => a._1 -> BigInt(hasher.hash64(a._2))).sortBy(_._2).map(_._1).take(2)
  }



  /**
    * just creating the string that is going to be hashed
    * @param coord
    * @param nodeID
    * @return
    */
  def createStringToHash(coord: Coordinate, nodeID: Int): String = {
    coord.latitude + "," + coord.longitude + "," + nodeID
  }
  def createStringToHash(coord: Coordinate, nodeID: String): String = {
    coord.latitude + "," + coord.longitude + "," + nodeID
  }
}
