package muic.opl.model
import java.net.InetAddress
import scala.collection._

case class Cluster(slaves: mutable.Set[Node], coordinates: Vector[Coordinate], master: Node, var nextnodeid: Int, var currentnodeid: Int) {
  def isReady: Boolean = isMaster && slaves.size == 5
  def isMaster: Boolean = master.ip == InetAddress.getLocalHost.getHostAddress
  def isSlave: Boolean = slaves == null && master.ip != InetAddress.getLocalHost.getHostAddress
  def getCurrentNodeID: Int = this.currentnodeid
  def getNextNodeID: Int = this.nextnodeid
  def getMaster: Node = this.master
  def getAllNodeID: Seq[Node] = this.slaves.toSeq
  def getCoordinates: Vector[Coordinate] = this.coordinates
  def getSlaves: mutable.Set[Node] = this.slaves
  def setNextNodeID(i: Int): Unit = nextnodeid = i
  def setCurrentNodeID(i: Int): Unit = currentnodeid = i
}
