package muic.opl.model

sealed trait Direction {
  def OPPOSITE: Direction
}
object Direction {
  val directionMap = Map("north" -> NORTH, "south" -> SOUTH, "east" -> EAST, "west" -> WEST)
  case object NORTH extends Direction {
    override def OPPOSITE: Direction = SOUTH
    override def toString: String = "south"
  }
  case object SOUTH extends Direction {
    override def OPPOSITE: Direction = NORTH
    override def toString: String = "north"
  }
  case object EAST  extends Direction {
    override def OPPOSITE: Direction = WEST
    override def toString: String = "west"
  }
  case object WEST  extends Direction {
    override def OPPOSITE: Direction = EAST
    override def toString: String = "east"
  }
}
