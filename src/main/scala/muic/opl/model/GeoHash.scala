package muic.opl.model

import muic.opl.model.Direction.{NORTH, SOUTH, EAST, WEST}

import scala.collection.mutable

object GeoHash {

  private val MAX_BITS: Int = 64
  private val MAX_LENGTH: Int = 12
  private val NUM_BITS: Int = MAX_LENGTH * 5
  private val SIGNIFICANT_BIT_MASK: Long = 1l << (MAX_BITS - 1)
  private val BASE32: Array[Char] =
    Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p',
      'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
  private val BASE32_MAP: Map[Char, Int] = Map[Char, Int]() ++ BASE32.zip(0 to BASE32.length)

  private val heightDegrees: List[Double] = List.tabulate(13) { n ⇒
    180 / math.pow(2, 2.5 * n + (if (n % 2 == 0) 0.0 else -0.5))
  }

  private val widthDegrees: List[Double] = List.tabulate(13) { n ⇒
    180 / math.pow(2, 2.5 * n + (if (n % 2 == 0) -1.0 else -0.5))
  }

  def length: Int = MAX_LENGTH

  /**
    * Encode a coordinate to geohash
    *
    * @param coordinate
    * latitude, longitude
    * @return
    * base32 representation of hash
    */
  def encode(coordinate: Coordinate): String =
    encode(coordinate, MAX_LENGTH)

  /**
    * Encode a coordinate to geohash with padding of zeroes
    * @param coordinate
    * latitude, longitude
    * @return
    * base32 representation of hash
    */
  def encodePadded(coordinate: Coordinate): String =
    encodePadded(coordinate, MAX_LENGTH)

  /**
    * Encode a coordinate to geohash
    *
    * @param coordinate
    * latitude, longitude
    * @return
    * long representation of hash
    */
  def encodeToLong(coordinate: Coordinate): Long =
    encodeToLong(coordinate, MAX_LENGTH)

  private def encodeToLong(coordinate: Coordinate, length: Int): Long =
    encode(coordinate.latitude, coordinate.longitude, 0, 0, length, (-90, 90), (-180, 180))._1

  private def encode(coordinate: Coordinate, length: Int): String = {
    toBase32(encodeToLong(coordinate, length))
  }

  private def encodePadded(coordinate: Coordinate, length: Int): String = {
    toBase32(encodeToLong(coordinate, length), pad = true)
  }

  private def encode(latitude: Double, longitude: Double, res: Long, index: Int, hashLength: Int,
                     latRange: (Double, Double), lonRange: (Double, Double)): (Long, (Coordinate, Coordinate)) = {
    if (index == hashLength * 5) {
      (res << (MAX_BITS - index), (Coordinate(latRange._2, lonRange._1), Coordinate(latRange._1, lonRange._2)))
    }
    else if (index % 2 == 0) {
      val mid = (lonRange._1 + lonRange._2) / 2d
      if (longitude >= mid) {
        encode(latitude, longitude, (res << 1) | 0x01, index + 1, hashLength, latRange, (mid, lonRange._2))
      }
      else {
        encode(latitude, longitude, res << 1, index + 1, hashLength, latRange, (lonRange._1, mid))
      }
    }
    else {
      val mid = (latRange._1 + latRange._2) / 2d
      if (latitude >= mid)
        encode(latitude, longitude, (res << 1) | 0x01, index + 1, hashLength, (mid, latRange._2), lonRange)
      else
        encode(latitude, longitude, res << 1, index + 1, hashLength, (latRange._1, mid), lonRange)
    }
  }

  /**
    * Decode a geoHash to coordinates
    *
    * @param hash
    * base32 representation of geoHash
    * @return
    * coordinates
    */
  def decode(hash: String): Coordinate = {
    decode(toLong(hash))
  }

  /**
    * Decode a geoHash to coordinates
    *
    * @param hash
    * long representation of geoHash
    * @return
    * coordinates
    */
  def decode(hash: Long): Coordinate = decode(hash, 0, (-90, 90), (-180, 180))

  private def decode(hash: Long, index: Int, latRange: (Double, Double), lonRange: (Double, Double)): Coordinate = {
    if (index == NUM_BITS) Coordinate((latRange._1 + latRange._2) / 2d, (lonRange._1 + lonRange._2) / 2d)
    else if (index % 2 == 0) {
      if ((hash & SIGNIFICANT_BIT_MASK) == SIGNIFICANT_BIT_MASK)
        decode(hash << 1, index + 1, latRange, ((lonRange._1 + lonRange._2) / 2d, lonRange._2))
      else
        decode(hash << 1, index + 1, latRange, (lonRange._1, (lonRange._1 + lonRange._2) / 2d))
    } else {
      if ((hash & SIGNIFICANT_BIT_MASK) == SIGNIFICANT_BIT_MASK)
        decode(hash << 1, index + 1, ((latRange._1 + latRange._2) / 2d, latRange._2), lonRange)
      else
        decode(hash << 1, index + 1, (latRange._1, (latRange._1 + latRange._2) / 2d), lonRange)
    }
  }

  /**
    * Convert geoHash representation from long to base32
    *
    * @param hash
    * long representation of geoHash
    * @return
    * base32 representation of geoHash
    */
  def toBase32(hash: Long, pad: Boolean = false): String = {
    val binaryStr = (hash >>> (MAX_BITS - NUM_BITS)).toBinaryString
    if (pad) toBase32(binaryStr.reverse.padTo(NUM_BITS, '0').reverse.iterator, "") else
    toBase32(binaryStr.iterator, "")
  }

  private def toBase32(bits: Iterator[Char], res: String): String = {
    if (!bits.hasNext) res
    else {
      val char = bits.take(5).toList
      val total: Int = char.zip(char.size - 1 to -1 by -1).foldLeft(0)((b, c) ⇒ {
        b + (c._1.toString.toInt * (1 << c._2))
      })
      toBase32(bits.drop(5), res + BASE32(total))
    }
  }

  /**
    * Convert geoHash representation from base32 to long
    *
    * @param hash
    * base32 representation of geoHash
    * @return
    * long representation of geoHash
    */
  def toLong(hash: String): Long = {
    val chars: Seq[Int] = hash.flatMap(BASE32_MAP.get)
    val h = chars.foldLeft(0l) { (r, c) ⇒
      r << 5 | c
    }
    h << (MAX_BITS - NUM_BITS)
  }

  /**
    * Whether a hash covers a coordinate
    *
    * @param hash
    * base32 representation of geoHash
    * @param coordinate
    * latitude, longitude
    * @return
    * true if coordinate falls within the geoHash zone
    */
  def hashContains(hash: String, coordinate: Coordinate): Boolean = {
    val centre = decode(hash)
    math.abs(centre.latitude - coordinate.latitude) <= (heightDegrees(hash.length) / 2) &&
      math.abs(centre.longitude - coordinate.longitude) <= (widthDegrees(hash.length) / 2)
  }

  private def hashLengthForBoundingBox(topLeft: Coordinate, botRight: Coordinate, index: Int,
                                       latRange: (Double, Double), lonRange: (Double, Double)): Int = {
    if (index == NUM_BITS)
      index
    else if (index % 2 == 0) {
      val mid = (lonRange._1 + lonRange._2) / 2d
      if (topLeft.longitude >= mid) {
        if (botRight.longitude < mid)
          index / 5
        else
          hashLengthForBoundingBox(topLeft, botRight, index + 1, latRange, (mid, lonRange._2))
      }
      else {
        if (botRight.longitude >= mid)
          index / 5
        else
          hashLengthForBoundingBox(topLeft, botRight, index + 1, latRange, (lonRange._1, mid))
      }
    }
    else {
      val mid = (latRange._1 + latRange._2) / 2d
      if (topLeft.latitude >= mid)
        if (botRight.latitude < mid)
          index / 5
        else
          hashLengthForBoundingBox(topLeft, botRight, index + 1, (mid, latRange._2), lonRange)
      else if (botRight.latitude >= mid)
        index / 5
      else
        hashLengthForBoundingBox(topLeft, botRight, index + 1, (latRange._1, mid), lonRange)
    }
  }

  protected[model] def coverStripBoundingBox(coordinate1: Coordinate, coordinate2: Coordinate, direction: Direction): List[String] = {
    direction match {
      case NORTH ⇒ coverBoundingBoxMax(Coordinate(90, math.min(coordinate1.longitude, coordinate2.longitude)),
        Coordinate(coordinate1.latitude, math.max(coordinate1.longitude, coordinate2.longitude)))
      case SOUTH ⇒ coverBoundingBoxMax(Coordinate(coordinate1.latitude, math.min(coordinate1.longitude, coordinate2.longitude)),
        Coordinate(-90, math.max(coordinate1.longitude, coordinate2.longitude)))

      case EAST ⇒ coverBoundingBoxMax(Coordinate(math.max(coordinate1.latitude, coordinate2.latitude), coordinate1.longitude),
        Coordinate(math.min(coordinate1.latitude, coordinate2.latitude), 180))
      case WEST ⇒ coverBoundingBoxMax(Coordinate(math.max(coordinate1.latitude, coordinate2.latitude), -180),
        Coordinate(math.min(coordinate1.latitude, coordinate2.latitude), coordinate1.longitude))
    }
  }

  protected[model] def coverBoundingBoxMax(topLeft: Coordinate, botRight: Coordinate): List[String] = {
    val startLength: Int = hashLengthForBoundingBox(topLeft, botRight, 0, (-90, 90), (-180, 180))
    coverBoundingBoxMax(topLeft, botRight, 12, math.max(1, startLength), Nil)
  }

  private def coverBoundingBoxMax(topLeft: Coordinate, botRight: Coordinate, maxHashes: Int, length: Int,
                                  coverage: List[String]): List[String] = {
    if (length > MAX_LENGTH)
      coverage
    else {
      val c: List[String] = coverBoundingBox(topLeft, botRight, length)
      if (c.size > maxHashes) {
        coverage match {
          case Nil ⇒ Nil
          case _ ⇒ coverage
        }
      }
      else
        coverBoundingBoxMax(topLeft, botRight, maxHashes, length + 1, c)
    }
  }

  private def coverBoundingBox(topLeft: Coordinate, botRight: Coordinate, length: Int = 12): List[String] = {
    val widthDegreesPerHash = widthDegrees(length)
    val heightDegreesPerHash = heightDegrees(length)

    val hashes = mutable.Set[String]()

    val diff = topLeft.longitudeDiff(botRight)
    val maxLon = topLeft.longitude + diff
    (botRight.latitude to topLeft.latitude by heightDegreesPerHash).foreach(
      lat ⇒ (topLeft.longitude to maxLon by widthDegreesPerHash).foreach(
        lon ⇒ {
          hashes += encode(Coordinate(lat, lon), length).substring(0, length)
        }))
    (botRight.latitude to topLeft.latitude by heightDegreesPerHash).foreach(
      lat ⇒ hashes += encode(Coordinate(lat, maxLon), length).substring(0, length))
    (topLeft.longitude to maxLon by widthDegreesPerHash).foreach(
      lon ⇒ hashes += encode(Coordinate(topLeft.latitude, lon), length).substring(0, length))
    hashes += encode(Coordinate(topLeft.latitude, maxLon), length).substring(0, length)

    hashes.toList
  }

  protected[model] def rectangularFilter(topLeft: Coordinate, botRight: Coordinate): Coordinate ⇒ Boolean =
    coordinate ⇒ coordinate.latitude >= botRight.latitude && coordinate.latitude < topLeft.latitude &&
      coordinate.longitude > topLeft.longitude && coordinate.longitude <= botRight.longitude

  protected[model] def radiusFilter(centre: Coordinate, radius: Double): Coordinate ⇒ Boolean =
    coordinate ⇒ centre.distance(coordinate) <= radius

  protected[model] def stripFilter(cord1: Coordinate, cord2: Coordinate, direction: Direction): Coordinate ⇒ Boolean = {
    direction match {
      case NORTH ⇒ coordinate =>
        coordinate.latitude >= cord1.latitude &&
          coordinate.longitude >= math.min(cord1.longitude, cord2.longitude) &&
          coordinate.longitude < math.max(cord1.longitude, cord2.longitude)
      case SOUTH ⇒ coordinate =>
        coordinate.latitude < cord1.latitude &&
          coordinate.longitude >= math.min(cord1.longitude, cord2.longitude) &&
          coordinate.longitude < math.max(cord1.longitude, cord2.longitude)

      case EAST ⇒ coordinate =>
        coordinate.longitude >= cord1.longitude &&
          coordinate.latitude >= math.min(cord1.latitude, cord2.latitude) &&
          coordinate.latitude < math.max(cord1.latitude, cord2.latitude)
      case WEST ⇒ coordinate =>
        coordinate.longitude < cord1.longitude &&
          coordinate.latitude >= math.min(cord1.latitude, cord2.latitude) &&
          coordinate.latitude < math.max(cord1.latitude, cord2.latitude)
    }
  }
}
