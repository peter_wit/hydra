package muic.opl.model
case class Coordinate(latitude: Double, longitude: Double) {
  require(latitude >= -90 && latitude <= 90, "Invalid Latitude")
  require(longitude >= -180 && longitude <= 180, "Invalid Longitude")

  def longitudeDiff(that: Coordinate): Double =
    math.abs(this.longitude - that.longitude)
  def latitudeDiff(that: Coordinate): Double =
    math.abs(this.latitude - that.latitude)

  def diff(that: Coordinate): Coordinate =
    Coordinate(latitudeDiff(that), longitudeDiff(that))

  def == (that: Coordinate): Boolean = {
    val d = diff(that)
    d.longitude == 0 && d.latitude == 0
  }

  override def toString: String = latitude + "," + longitude

  /**
    * Distance function using Vincenty's formula
    * @param that
    *             another coordinate
    * @return
    *         distance in kilometers to `that` coordinate
    */
  def distance(that: Coordinate): Double = {
    val deltaLon: Double = that.longitude.toRadians - longitude.toRadians
    val cosLat1: Double = math.cos(latitude.toRadians)
    val cosLat2: Double = math.cos(that.latitude.toRadians)
    val sinLat1: Double = math.sin(latitude.toRadians)
    val sinLat2: Double = math.sin(that.latitude.toRadians)
    val cosDeltaLon: Double = math.cos(deltaLon)
    val top: Double = math.sqrt(math.pow(cosLat2 * math.sin(deltaLon),2) +
      math.pow(cosLat1 * sinLat2 - sinLat1 * cosLat2 * cosDeltaLon, 2))
    val bot: Double = sinLat1 * sinLat2 + cosLat1 * cosLat2 * cosDeltaLon
    math.abs(Coordinate.earth_radius * math.atan2(top, bot))
  }
}

object Coordinate {
  val earth_radius: Double = 6371.01
}
