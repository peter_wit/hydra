package muic.opl.model
import muic.opl.model.Direction.{EAST, NORTH, SOUTH, WEST}
import xyz.hyperreal.btree.MemoryBPlusTree

import scala.collection.mutable

object Database {
  val map: mutable.LongMap[MemoryBPlusTree[Long, Record]] =
    new mutable.LongMap[MemoryBPlusTree[Long, Record]]()

  def insert(record: Record): Unit = {
    insert(GeoHash.encodePadded(record.coordinate), record, 1)
  }

  private def insert(geoHash: String, record: Record, it: Int): Unit = {
    if (it > GeoHash.length - 2) Unit
    else {
      val key = GeoHash.toLong(geoHash.substring(0, it))
      map.getOrElseUpdate(key, new MemoryBPlusTree[Long, Record](256)).insert(GeoHash.encodeToLong(record.coordinate), record)
      insert(geoHash, record, it+1)
    }
  }

  def get(coordinate: Coordinate): Option[Record] = {
    val bucket = GeoHash.toLong(GeoHash.encodePadded(coordinate).substring(0, GeoHash.length - 2))
    val long = GeoHash.encodeToLong(coordinate)
    map.get(bucket) match {
      case Some(t) ⇒ t.search(long)
      case None ⇒ None
    }
  }

  def findWithinBox(topLeft: Coordinate, botRight: Coordinate): List[Record] = {
    val cov = GeoHash.coverBoundingBoxMax(topLeft, botRight)
    cov.flatMap(x ⇒ findWithin(x, rectangularFilter(topLeft, botRight)))
  }

  def findWithinCircle(centre: Coordinate, radius: Double): List[Record] = {
    val topLeftLat = centre.latitude + (radius/111d)
    val topLeftLon = centre.longitude - (radius/(111d * math.cos(topLeftLat.toRadians)))
    val botRightLat = centre.latitude - (radius/111d)
    val botRightLon = centre.longitude + (radius/(111d * math.cos(botRightLat.toRadians)))
    val cov = GeoHash.coverBoundingBoxMax(Coordinate(topLeftLat, topLeftLon),
      Coordinate(botRightLat, botRightLon))
    cov.flatMap(x ⇒ findWithin(x, radiusFilter(centre, radius)))
  }

  def findWithinStrip(coordinate1: Coordinate, coordinate2: Coordinate, direction: Direction): List[Record] = {
    require((coordinate1.latitude == coordinate2.latitude && (direction == NORTH || direction == SOUTH)) ||
      (coordinate1.longitude == coordinate2.longitude && (direction == EAST || direction == WEST)))
    val cov = GeoHash.coverStripBoundingBox(coordinate1, coordinate2, direction)
    cov.flatMap(x ⇒ findWithin(x, stripFilter(coordinate1, coordinate2, direction)))
  }

  private def findWithin(withinHash: String, filter: Record ⇒ Boolean): List[Record] = {
    val key = GeoHash.toLong(withinHash)
    val v = map.get(key)
    v match {
      case Some(va) ⇒ va.valuesIterator.filter(filter).toList
      case None ⇒ Nil
    }
  }

  private def rectangularFilter(topLeft: Coordinate, botRight: Coordinate): Record ⇒ Boolean =
    record ⇒ GeoHash.rectangularFilter(topLeft, botRight)(record.coordinate)

  private def radiusFilter(centre: Coordinate, radius: Double): Record ⇒ Boolean =
    record ⇒ GeoHash.radiusFilter(centre, radius)(record.coordinate)

  private def stripFilter(cord1: Coordinate, cord2: Coordinate, direction: Direction): Record ⇒ Boolean =
    record ⇒ GeoHash.stripFilter(cord1, cord2, direction)(record.coordinate)
}