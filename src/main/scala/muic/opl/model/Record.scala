package muic.opl.model

case class Record(coordinate: Coordinate, timeStamp: Long, payLoad: Array[Byte]){

  override def toString: String = s"""[Latitude: ${coordinate.latitude}, Longitude: ${coordinate.longitude}]"""
}
