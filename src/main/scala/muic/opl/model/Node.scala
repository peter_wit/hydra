package muic.opl.model

case class Node(ip: String, port: String){
  override def toString: String = ip + ":" + port
  def ==(n: Node): Boolean = this.ip == n.ip && n.port == this.port
}