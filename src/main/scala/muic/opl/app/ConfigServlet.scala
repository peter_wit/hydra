package muic.opl.app

import org.json4s.{DefaultFormats, Formats}
import org.scalatra.{CorsSupport, ScalatraServlet}
import org.scalatra.json.JacksonJsonSupport

class ConfigServlet extends ScalatraServlet with JacksonJsonSupport with CorsSupport  {
  implicit val jsonFormats: Formats = DefaultFormats
  before() {
    contentType = formats("json")
  }
  /**
    * create the config file (normally this wont be called, config is only created during the creation of the master
    */
  post("/create/master"){
    "Config creation - this is probably not needed for master"
  }

  /**
    *
    */
  post("/create/slave"){
    "Config creation - basically a command to go and get config from the master - probably not needed too"
  }

  /**
    *
    */
  get("/get"){
    "Config get "
    //    val key = params("key")
  }
}
