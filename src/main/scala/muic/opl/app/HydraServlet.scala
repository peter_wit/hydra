package muic.opl.app

import muic.opl.service.{ClusterService, ShardingService}
import org.scalatra._
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json._
import muic.opl.hash._
import muic.opl.model.Cluster
import java.net._
import muic.opl.model.Node

/**
  * We are focusing on the this
  * key -> latitude, longitude
  * ts -> timestamp
  * payload -> bytes
  * make up a record (key,ts,payload)
  * ts will not be provided -> we make that
  */
class HydraServlet extends ScalatraServlet with JacksonJsonSupport with CorsSupport {
  implicit val jsonFormats: Formats = DefaultFormats

  options("/*"){
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
  }

  before() {
    contentType = formats("json")
  }

  post("/set/information"){
    val jsonString= request.body
    try{
      if(!jsonString.isEmpty){
        val jValue = parse(jsonString)
        val info = jValue.extract[Node]
        ServletItem.ip = info.ip
        ServletItem.port = info.port.toInt
        ServletItem.jsonService.makeSuccess()
      }else{
        ServletItem.jsonService.makeFailure("Dead")
      }
    }catch {
      case _: Exception => "Oppsssss we died"
    }
  }
  post("/set/ip"){
    val jsonString= request.body
    try{
      if(!jsonString.isEmpty){
        val jValue = parse(jsonString)
        val info = jValue.extract[(String, String)]
        ServletItem.ip = info._1
        ServletItem.jsonService.makeSuccess()
      }else{
        ServletItem.jsonService.makeFailure("Dead")
      }
    }catch {
      case _: Exception => "Oppsssss we died"
    }
  }
  post("/set/port"){
    val jsonString= request.body
    try{
      if(!jsonString.isEmpty){
        val jValue = parse(jsonString)
        val info = jValue.extract[(String, Int)]
        ServletItem.port = info._2
        ServletItem.jsonService.makeSuccess()
      }else{
        ServletItem.jsonService.makeFailure("Dead")
      }
    }catch {
      case _: Exception => "Oppsssss we died"
    }
  }

  notFound {
    NotFound("Sorry, the application does not support the API you are giving")
  }
}
