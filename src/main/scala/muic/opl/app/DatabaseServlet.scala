package muic.opl.app

import java.util.NoSuchElementException

import muic.opl.exception._
import muic.opl.model.{Coordinate, Node, Record}
import org.json4s.{DefaultFormats, Formats, _}
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.{CorsSupport, ScalatraServlet}
class DatabaseServlet extends ScalatraServlet with JacksonJsonSupport with CorsSupport{
  implicit val jsonFormats: Formats = DefaultFormats
  options("/*"){
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
  }

  before() {
    contentType = formats("json")
  }

  /**
    * takes 2 coordinates with either same longitude or latitude and the direction
    */
  get("/geostrip"){
    try{
      //println("converting")
      val c1 = request.header("location1").get.split(",")
      val c2 = request.header("location2").get.split(",")
      val coord1 = Coordinate(c1(0).toDouble,c1(1).toDouble)
      val coord2 = Coordinate(c2(0).toDouble,c2(1).toDouble)
      val direction = ServletItem.directionParser(request.header("direction").get)
      //println("getting records")
      val records = ServletItem.databaseService.geoStrip(coord1,coord2,direction)
      //println(records)
      //println("Translation done")
      if(ServletItem.cluster.isMaster) {
        //println("sedning all")
        val result = ServletItem.cluster
          .getSlaves
          .flatMap(node => ServletItem.communicationService.sendDatabaseGeostripRequestToSlaves(node,request.header("location1").get,request.header("location2").get,direction.toString))
          .toList
        //println("after asking for all: checking for everything")
        (records, result) match {
          case (Nil, Nil) => ServletItem.jsonService.makeFailure("No Record of such location found")
          case (Nil, h :: t) => ServletItem.jsonService.makeJsonFromRecordsString(h::t)
          case (h :: t, Nil) => ServletItem.jsonService.makeJsonFromRecords(h :: t)
          case (a :: b, c :: d) => {
            //println("got all records")
            val rc = ServletItem.jsonService.parseResponses(ServletItem.jsonService.makeJsonFromRecords(a :: b))._2
            val extendedList = (c :: d) ::: rc
            ServletItem.jsonService.makeJsonFromRecordsString(extendedList.toSet.toList)
          }
        }
      }else{
        records match {
          case Nil => ServletItem.jsonService.makeFailure("No Record of such location found")
          case h::t => ServletItem.jsonService.makeJsonFromRecords(records)
        }
      }
    }catch {
      case c: IllegalArgumentException => ServletItem.jsonService.makeFailure("Coordinates have to be in double or integer.")
//      case c: NoSuchElementException => ServletItem.jsonService.makeFailure("")
      case _: Throwable => ServletItem.jsonService.makeFailure("Unknown Exception: Please specify coordinates with either same latitude or same longitude and the actual direction")
    }
  }

  /**
    * format header
    * location1 -> northwest
    * location2 -> southeast
    * location1 -> latitude,longitude
    * location2 -> latitude,longitude
    * return -> coordinates
    */
  get("/geobox"){
    try{
      //println("converting")
      val c1 = request.header("location1").get.split(",")
      val c2 = request.header("location2").get.split(",")
      val coord1 = Coordinate(c1(0).toDouble,c1(1).toDouble)
      val coord2 = Coordinate(c2(0).toDouble,c2(1).toDouble)
      //println("getting records")
      val records = ServletItem.databaseService.geoBox(coord1,coord2)
      //println(records)
      //println("Translation done")
      if(ServletItem.cluster.isMaster) {
        val result = ServletItem.cluster
          .getSlaves
          .flatMap(node => ServletItem.communicationService.sendDatabaseGeoboxRequestToSlaves(node,request.header("location1").get,request.header("location2").get))
          .toList
        (records, result) match {
          case (Nil, Nil) => ServletItem.jsonService.makeFailure("No Record of such location found")
          case (Nil, h::t) => ServletItem.jsonService.makeJsonFromRecordsString(h::t)
          case (h::t, Nil) => ServletItem.jsonService.makeJsonFromRecords(h::t)
          case (a::b, c::d) => {
            val rc = ServletItem.jsonService.parseResponses(ServletItem.jsonService.makeJsonFromRecords(a::b))._2
            val extendedList = (c::d):::rc
            ServletItem.jsonService.makeJsonFromRecordsString(extendedList)
          }
        }
      }else{
        records match {
          case Nil => ServletItem.jsonService.makeFailure("No Record of such location found")
          case h::t => ServletItem.jsonService.makeJsonFromRecords(records)
        }
      }
    }catch {
      case c: IllegalArgumentException => ServletItem.jsonService.makeFailure("Coordinates have to be in double or integer")
      case c: NoSuchElementException => ServletItem.jsonService.makeFailure("Please specify the top left of the box and the bottom right of the box")
      case _: Throwable => ServletItem.jsonService.makeFailure("Unknown Exception: Possible that the locations aren't in number or are too long")
    }
  }

  /**
    * json format
    * location -> latitude,longitude
    * radius -> double
    */
  get("/georadius"){
    try{
      val c = request.header("location").get.split(",")
      val r = request.header("radius").get.toDouble
      //println("starting")
      val coord = Coordinate(c(0).toDouble,c(1).toDouble)
      //println(coord.toString)
      val records = ServletItem.databaseService.geoRadius(coord,r)
      //println(records.toString() + " " + records.length)
      if(ServletItem.cluster.isMaster) {
        val result = ServletItem.cluster
          .getSlaves
          .flatMap(node => ServletItem.communicationService.sendDatabaseGeoradiusRequestToSlaves(node,request.header("location").get,r.toString))
          .toList
        (records, result) match {
          case (Nil, Nil) => ServletItem.jsonService.makeFailure("No Record of such location found")
          case (Nil, h::t) => ServletItem.jsonService.makeJsonFromRecordsString(h::t)
          case (h::t, Nil) => ServletItem.jsonService.makeJsonFromRecords(h::t)
          case (a::b, c::d) => {
            val rc = ServletItem.jsonService.parseResponses(ServletItem.jsonService.makeJsonFromRecords(a::b))._2
            val extendedList = (c::d):::rc
            ServletItem.jsonService.makeJsonFromRecordsString(extendedList)
          }
        }
      }else{
        records match {
          case Nil => ServletItem.jsonService.makeFailure("No Record of such location found")
          case h::t => ServletItem.jsonService.makeJsonFromRecords(records)
        }
      }
    }catch {
      case c: NoSuchElementException => ServletItem.jsonService.makeFailure("Please specify the location and the search radius")
      case _: Throwable => ServletItem.jsonService.makeFailure("Unknown Exception: Possible that the locations aren't in number or are too long. Radius should be a number not too long in length")
    }
  }

  /**
    * get the record from the database
    * format header:
    * location -> latitude,longtitude
    */
  get("/get"){
    try{
      val c = request.header("location").get.split(",")
      //println(c(0) + " " + c(1))
      val coord = Coordinate(c(0).toDouble,c(1).toDouble)
      //println("find coord: " + coord)
      val record = ServletItem.databaseService.get(coord)
      //println("record: " + record)
      //basically if record is None and the cluster is master, go look for the record in other nodes
      if (ServletItem.cluster.isMaster && record.isEmpty){
        //println("record not in master -> sending requests to other nodes")
        val result = ServletItem.cluster.getSlaves.toList.map(node => ServletItem.communicationService.sendDatabaseGetRequestToSlaves(node,coord.toString))
        //println("results: " + result)
        val filtered = result.filter(_ != "")
        //println("filtered: " + filtered)
        filtered match {
          case Nil => throw new NoRecordFoundException
          case h::t => h
        }
      }else{
        record match {
          case None => throw new NoRecordFoundException
          case r => ServletItem.jsonService.makeJsonFromRecord(r.get)
        }
      }
    }catch {
      case c: NoSuchElementException => ServletItem.jsonService.makeFailure("You did not enter the location")
      case c: NoRecordFoundException => ServletItem.jsonService.makeFailure("No such coordinate is found")
      case e: Throwable =>
        e.printStackTrace()
        ServletItem.jsonService.makeFailure("Unknown error, most likely the format is not in latitude,longtitude")
    }
  }

  /**
    * check the ususal empty json body
    * then create the record from the body
    * if the servlet is the master, determine who to send it to and send it to those nodes
    * if the servlet is the slave, just wait insert the record into the database
    * json format
    * latitude,longtitude -> List[String]
    */
  post("/add"){
    val jsonString = request.body.toString
    //println("json body: "+ jsonString)
    //println(jsonString.isEmpty)
    try{
      if(!jsonString.isEmpty){
        val jValue = parse(jsonString)
        val recordString = jValue.extract[(String, List[String])]
        //println("making crd")
        val crd = recordString._1.split(",").map(_.toDouble)
        //println("making coordinate")
        val coordinate = Coordinate(crd(0),crd(1))
        //println("making record")
        //change the List[String] into String with space between and then convert them to byte
        val r2 = recordString._2.foldLeft("")((str,e) => str + " " + e).trim
        val record = Record(coordinate,System.currentTimeMillis(),r2.toCharArray.map(_.toByte))
        //println("made rec")
        if (ServletItem.cluster.isMaster){
          //println("master sending item to slaves")
          //determine which nodes -> send to those nodes
          //println(ServletItem.cluster.getSlaves)
          val nodes = ServletItem.shardingService.determineRecordLocation(coordinate, ServletItem.cluster.getSlaves.toVector)
          //println(nodes)
          if (nodes.contains(Node(ServletItem.ip, ServletItem.port.toString)))
            ServletItem.databaseService.insert(record)
          nodes.toVector.par.foreach(a => ServletItem.communicationService.sendRecordToNode(a,recordString._1,recordString._2))
          ServletItem.jsonService.makeSuccess()
        }else if(ServletItem.cluster.isSlave){
          //println("Slaves receiving record to insert: " + record.coordinate)
          ServletItem.databaseService.insert(record)
          ServletItem.jsonService.makeSuccess()
        }else{
          /**
            * neither a master nor slave -> just do stuff normally
            */
//          //println("received -> " + record.toString)
//          ServletItem.databaseService.insert(record)
//          ServletItem.jsonService.makeSuccess()
          ServletItem.jsonService.makeFailure("The cluster is not ready yet")
        }
      }else{
        ServletItem.jsonService.makeFailure("Empty request body")
      }
    }catch {
      case c: NoSuchElementException => ServletItem.jsonService.makeFailure("You did not enter the location")
      case c: IllegalArgumentException => ServletItem.jsonService.makeFailure("Latitude must be between 90 and -90 while Longitude must be between 180 and -180. ")
      case _: Throwable => ServletItem.jsonService.makeFailure("Unknown error, most likely the wrong format for the input")
    }
  }
}
