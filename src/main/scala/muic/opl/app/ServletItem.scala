package muic.opl.app

import java.net.InetAddress

import muic.opl.hash.HashCode
import muic.opl.model._
import muic.opl.service._

object ServletItem {

  var shardTable = Map()
  var cluster = Cluster(scala.collection.mutable.HashSet(), Vector(), Node("",""), -1, -1)
  var ip = InetAddress.getLocalHost.getHostAddress
  var port = 8090
  val shardingService = new ShardingService with HashCode
  val clusterService = new ClusterService
  val configService = new ConfigService
  val databaseService = new DatabaseService
  val jsonService = new JsonService
  val communicationService = new CommunicationService
  val directionParser = Direction.directionMap
}
