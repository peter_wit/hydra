package muic.opl.app
import org.scalatra._
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json._
import org.json4s._
import org.json4s.JsonDSL._
import java.net._
import muic.opl.model.Node
class ClusterServlet extends ScalatraServlet with JacksonJsonSupport with CorsSupport {
  implicit val jsonFormats: Formats = DefaultFormats

  options("/*"){
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
  }


  before() {
    contentType = formats("json")
  }
  /**
    * Create a cluster with this node as the master
    * TODO:
    * Get the bunch of cords from the database and update the cluster and create the shard table and store it.
    */
  post("/create") {
    val jsonString = request.body
    println("creating cluster")
    try{

      if (!request.body.isEmpty && !ServletItem.cluster.isSlave && !ServletItem.cluster.isMaster) {
        println("valid cluster creation")
        val jValue = parse(jsonString)
        println("parsed json")
        val ip = jValue.extract[(String, List[Node])]
        println("creating master")
        ServletItem.cluster = ServletItem.clusterService.createMasterCluster()
        println("sending requests to the slaves")
        ip._2.toVector.par.foreach(node => ServletItem.communicationService.checkValidNode(node))
        println("successful slaves notification")
        ServletItem.jsonService.makeSuccess()
      }else{
        println("invalid cluster creation")
        ServletItem.jsonService.makeFailure("Server is not ready")
      }
    } catch {
      case _: Exception => ServletItem.jsonService.makeFailure("Server is not ready")
    }
  }

//  post("/add") {
//    val jsonString = request.body
//    try {
//      if (!request.body.isEmpty && ServletItem.cluster.isMaster) {
//        val jValue = parse(jsonString)
//        val node  = jValue.extract[Node]
//        ServletItem.clusterService.addSlave(ServletItem.cluster,node)
//        ServletItem.jsonService.makeSuccess()
//      }
//    } catch {
//      case _: Exception => "Server is not ready"
//    }
//  }

  /**
    * FOR SLAVE
    * Join a cluster to become a slave
    * Require the ip of the master
    * Send him your ip and the amt of coordinates you have.
    * Assumption: WE ASSUME THAT THE SLAVE HAS NO ITEM INSIDE
    */
  post("/join/slave"){
    val jsonString = request.body
    try {
      println("join slaves notification")
      if (!request.body.isEmpty && !ServletItem.cluster.isMaster && !ServletItem.cluster.isSlave) {
        println("valid slave creation")
        val jValue = parse(jsonString)
        val master = jValue.extract[Node]
        println("creating slave")
        ServletItem.cluster = ServletItem.clusterService.createSlaveCluster(master)
        println("reporting to master")
        ServletItem.communicationService.reportToMaster(master)
        println("reported")
        ServletItem.jsonService.makeSuccess()
      }else{
        ServletItem.jsonService.makeFailure("Server is not ready")
      }
    }catch {
      case _: Exception => ServletItem.jsonService.makeFailure("Server is not ready")
    }

  }

  /**
    * FOR MASTER
    * Received when someone is trying to join your cluster
    * You have to be the master to be able to received validate this join
    * Otherwise the you return it 404 or some error
    */
  post("/join/master"){
    val jsonString = request.body
    try{
      println("master receviing join requests from slaves")
      if (!request.body.isEmpty && !ServletItem.cluster.isReady && ServletItem.cluster.isMaster) {
        val jValue = parse(jsonString)
        println("parsing json")
        val node = jValue.extract[Node]
        println("adding slaves into cluster")
        ServletItem.clusterService.addSlave(ServletItem.cluster,node)
        println("adding slaves into cluster")
        ServletItem.jsonService.makeSuccess()
      } else {
        ServletItem.jsonService.makeFailure("Server is not ready")
      }
    }catch {
      case _:Exception => ServletItem.jsonService.makeFailure("Server is not ready")
    }

  }
}
