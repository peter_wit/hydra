package muic.opl.hash


trait FNV1AHash extends FNVHash {
  override def hash64(toHash: String): String = {
    val sbytes = toHash.getBytes
    val offset = FNV_offset_basis_64
    val prime = FNV_prime_64
    sbytes.foldLeft(offset)((hash, element) => (hash ^ BigInt(element)) *  prime).toString
  }

  override def hash32(toHash: String) = ???
}
