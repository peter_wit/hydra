package muic.opl.hash


trait FNV1Hash extends FNVHash {

  override def hash64(toHash: String): String = {
    val sbytes = toHash.getBytes
    val offset = FNV_offset_basis_64
    val prime = FNV_prime_64
    sbytes.foldLeft(offset)((hash, element) => (hash * prime) ^ BigInt(element)).toString
  }

  override def hash32(toHash: String) = ???
}
