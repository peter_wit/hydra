package muic.opl.hash

trait HashService {
  def hash32(toHash: String): String

  def hash64(toHash: String): String
}
