package muic.opl.hash

trait HashCode extends HashService {
  override def hash32(toHash: String) = toHash.hashCode.toString

  override def hash64(toHash: String) = toHash.hashCode.toString
}
