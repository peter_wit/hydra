package muic.opl.hash

trait FNVHash extends HashService {
  val FNV_offset_basis_64 = BigInt("14695981039346656037")
  val FNV_prime_64 = BigInt("1099511628211")
}
