package muic.opl.hash
import scala.language.postfixOps
import com.roundeights.hasher.Implicits._

trait MD5Hash extends HashService {
  override def hash32(toHash: String) = toHash.md5.hex

  override def hash64(toHash: String) = toHash.md5.hex
}
