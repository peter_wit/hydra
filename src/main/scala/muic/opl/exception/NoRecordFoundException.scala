package muic.opl.exception

class NoRecordFoundException extends Exception {
  val _NO_RECORD_FOUND = "NO_RECORD_FOUND"
}
