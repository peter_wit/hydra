import muic.opl.app._
import org.scalatra._
import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new HydraServlet, "/*")
    context.mount(new ClusterServlet, "/cluster")
    context.mount(new DatabaseServlet, "/database")
    context.mount(new ConfigServlet, "/config")
  }
}
