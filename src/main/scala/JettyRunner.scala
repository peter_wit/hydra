import muic.opl.app.HydraServlet
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener

import scala.io.Source

object JettyRunner extends App{
  def run(): Unit = {
    def readProperties(): Map[String,String] = {
      val properties = Source.fromResource("general.properties").getLines
      properties.map(line => line.split("=")).map((arr) => arr(0) -> arr(1)).toMap
    }
    val properties = readProperties()
    val port = properties.get("port")
    if (port.isEmpty)
      throw new Exception("Properties malformed")
    val server = new Server(port.get.toInt)
    val context = new WebAppContext()
    context setContextPath "/"
    context setResourceBase "src/main/app"
    context.addEventListener(new ScalatraListener)
    context.addServlet(classOf[HydraServlet], "/*")

    server.setHandler(context)

    server.start()
    server.join()
  }
  run()
}
