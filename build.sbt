val ScalatraVersion = "2.5.4"

organization := "muic.opl"

name := "Hydra"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.3"

resolvers ++= Seq(
  Classpaths.typesafeReleases,
  "Hyperreal Repository" at "https://dl.bintray.com/edadma/maven"
)

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.5" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.15.v20160210" % "container;compile",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "xyz.hyperreal" %% "b-tree" % "0.3",
  "org.json4s"   %% "json4s-jackson" % "3.5.2",
  "com.roundeights" %% "hasher" % "1.2.0",
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.apache.httpcomponents" % "httpclient" % "4.5.3",
)

assemblyJarName in assembly := "hydra.jar"
test in assembly := {}


enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)
enablePlugins(JettyPlugin)

containerPort in Jetty := 8090
