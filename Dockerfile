# Pull base image
FROM openjdk:8

COPY target/scala-2.12/hydra.jar .

RUN ["java", "-version"]

CMD ["java", "-jar", "hydra.jar"]