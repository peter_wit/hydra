#### How to use:
1. Clone this project
2. run command `./build.sh` then run command `docker build -t hydra .`
3. `docker-compose up`
    - This will create 5 dockers instances of this project
4. download postman and do a setup from <https://www.getpostman.com/collections/df289bc60c6c7ff2c11c>
5. run the requests in the setInfo folder from the postman collection, this is arguably a better way than using the 
   commands from the CLI to change the connection and change the information of that database
   - We will try to in future support mass set up from the CLI from a config file.
6. run the CreateCluster request in the cluster folder from the postman collection.
    - this uses the container names as the hostname of each node. If you change the name of the containers in the
      docker-compose file then you have to change the name used in postman too.
7. now the cluster have been set up, you can send the usual commands specified in the [CLI](https://bitbucket.org/peter_wit/hydracli) normally.
    - Clone the CLI: <https://bitbucket.org/peter_wit/hydra>
    - In the CLI repo, build the CLI with `./build.sh` and run it with `./run.sh`
    - can load some sample data from the CLI with `>database sampledata [full path to airport-extended_1.xml contained in CLI repo]`

We will continue making more feature for this project even after OPL class

#### TODO

1. Fix bugs 
2. Refactor the API and use design patterns
3. allow adding of nodes
4. allow death of nodes

#### Goal:

Create a distributed in-memory geo temporal database that support queries such as querying for a point in a radius, a point in a rectangle, a point in a 3 sided area (or a striped?). 

The database must be resilient to the loss of data in case a node die and the speed of the query must be acceptably fast, meaning almost as fast as a normal database.
Objective: Key-value In-Memory database Able to become a distributed system Use sharding as a storage technique Must be reasonable resilience against data losses Must be reasonably fast


#### Basic Structure
- _muic.opl.model_ contains the main components of Hydra e.g the Database, GeoHash and basic models for sharding.
- _muic.opl.service_ contains the public methods used by the REST servlets
- _muic.opl.app_ contains the Servlets with the REST mappings to control the system